# md编译器客户端

#### 介绍
md文档直接编译成 对应的代码,类似低代码 算是TP6的超级脚手架,
解决普通curd 机械劳动 浪费时间问题
里面包含自己改造的开源md编辑器, 感谢无私分享

#### 软件架构
软件架构说明
#### 更新说明
1. 增加了 指定模块路径功能 示例 {forcePath:sms}

#### 安装教程

1.  随便找个文件夹 clone 项目
2.  复制项目到根目录 别复制git
3.  本地运行 ok,不要放到服务器上运行 容易版本冲突

#### 使用说明

1.  保留 id,create_time,sort,is_enable,name 字段 基础调用用到了这四个
2.  如果想不用上面的字段 那么生成后 请自行改动对应代码
3.  file 字段为保留字段 请勿使用
4.  点击想要添加的字段 修改为你想要的 提交即可
5.  重新建表  在说明里 加入 {override},请在保证安全的前提下使用
6.  强制更新文件  在说明里 加入 {forceUpdate},请在保证安全的前提下使用
7.  每次更新文件 会在 app/client/sql 文件夹下生成sql文件 方面回溯
8.  克隆地址 https://gitee.com/saruri/client.git
9.  菜单 请自行建立  地址为 client/build/index 或者运行如下sql,会出现在系统管理菜单下 
```
INSERT INTO `system_menu` ( `pid`, `title`, `node`, `url`, `target`, `status` ) VALUES ( '2', '客户端', 'client/build/index', 'client/build/index', '_self', '1' );
```

#### 准备修复bug
1. 设置好路径后 swagger没有更新



#### 2023Raodmap

实装了 必填验证器 类型过滤器 长度过滤器等
