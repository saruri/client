<?php
declare(strict_types = 1);
// +----------------------------------------------------------------------
// | ThinkXinrun
// +----------------------------------------------------------------------
// | 版权所有 2020 唐山鑫软网络科技有限公司 [ http://www.tsxrwl.com ]
// +----------------------------------------------------------------------
// | 官方网站: https://www.tsxrwl.com
// +----------------------------------------------------------------------

namespace app\admin\libs;

use think\admin\Service as AdminService;
use think\admin\Controller;
use think\facade\Session;
use think\facade\Config;
use think\facade\Cache;

/**
 *  WhereService 创建的服务 构造where 数组 返回数组 常规满足不了的 就写下专用where
 * Class  WhereOrService
 * @package app\admin\service
 */
class WhereOrService extends AdminService
{
    protected $table = 'SystemWhereOrService';//表名
    // 初始化

    public $whereArr = [];//查询数组

    protected function initialize()
    {
        $this->ConfigData = Config::get('whereOr');//调用配置文件示例
        $this->isOr =false;
        if (empty($this->ConfigData)) {
            return [];
        }
        //halt($this->ConfigData);
    }
    
    /**
    * 默认
    * @author    saruri <saruri@163.com>
    * @date      2020-10-26 01:58:32
    * @auth true
    * @menu true
    * @login true
    * @throws \think\Exception
    * @throws \think\db\exception\DataNotFoundException
    * @throws \think\db\exception\DbException
    * @throws \think\db\exception\ModelNotFoundException
    */
    public function index()
    {
        
        $this->replace();
        if($this->isOr ==false){
            return false;
        }
        //exit("这是默认");
        // halt($this->whereArr);
       // halt($this->queryArr);
        return $this->queryArr;
    }

    /*
    *  @desc      导入查询数组
    *  @author    saruri <saruri@163.com>
    *  @date      2020/10/26 14:00:36
    */
    public function set(array $data)
    {
        //do somehing
        unset($data['spm']);
        unset($data['limit']);
        unset($data['page']);
        $this->queryArr=$data;
        //halt($data);
        return $this;
    }


    /**
    * 替换
    * @author    saruri <saruri@163.com>
    * @date      2021/09/08 16:47:51  
    * @auth true
    * @menu true
    * @login true
    * @throws \think\Exception
    * @throws \think\db\exception\DataNotFoundException
    * @throws \think\db\exception\DbException
    * @throws \think\db\exception\ModelNotFoundException
    */ 
    public function  replace()
    {
        
        foreach ($this->queryArr as $key => $value) {
            $k=$value[0];
            $value[0]=$this->getReplace($value[0]);
            $this->queryArr[$key]=$value;
        }
    }

    /**
    * 检查是否在配置数组内 存在就返回替换后的值
    * @author    saruri <saruri@163.com>
    * @date      2021/09/08 16:58:30  
    * @auth true
    * @menu true
    * @login true
    * @throws \think\Exception
    * @throws \think\db\exception\DataNotFoundException
    * @throws \think\db\exception\DbException
    * @throws \think\db\exception\ModelNotFoundException
    */ 
    public function  getReplace(string $val)
    {
        foreach ($this->ConfigData as $key => $value) {
            if($key==$val){
                $this->isOr=true;
                return $value;
            }
        }
        return $val;
         
    }


    /*
    *  @desc      得出默认数组
    *  @author    saruri <saruri@163.com>
    *  @date      2020/10/26 14:04:25
    */
    public function common()
    {
        return $this;
        //do somehing
        unset($this->queryArr['is_search']);
       
        //exit('==='.is_numeric($this->queryArr['teacher_name'])."测试".is_numeric($this->queryArr['sex']).'///'.is_numeric($this->queryArr['type']));
        foreach ($this->queryArr as $key => $value) {

            //halt($this->queryArr[$key]);
            //halt($key.'__'.$value);
            //获取0 1 之类的 布尔值
            if (self::getBool($key, $value)) {
                continue; //跳出本次循环
            }

            //获取时间范围 以 - 为判断
            if (self::getDate($key, $value)) {
                continue; //跳出本次循环
            }

            //获取6位以下数字
            if (self::getIntval($key, $value)) {
                continue; //跳出本次循环
            }


            //获取字符串
            if (self::getString($key, $value)) {
                continue; //跳出本次循环
            }
                       
            
            //其他待完善
            //self::($value);
        }
        //var_dump($this->queryArr);
        //halt($this->whereArr);
        return $this;
    }


    /*
    *  @desc      获取布尔
    *  @author    saruri <saruri@163.com>
    *  @date      2020/10/26 14:33:57
    */
    public function getBool($key, $value)
    {
        //do somehing
        //if (mb_strlen($value, 'UTF8')==1 and $value) {
        //$arr=[0,1,'0','1'];
        if ($value===0 or $value===1 or $value==='0' or $value==='1') {
            //if (in_array($value, $arr,true)) {
            // if ($value!='') {
            $this->whereArr[]=[$key,'=',$value];
            return true;
            //}
              
            //$this->whereArr[]='';
        //}
        }
       
        return false;
    }

    /*
    *  @desc      获取数字
    *  @author    saruri <saruri@163.com>
    *  @date      2020/10/26 14:34:41
    */
    public function getIntval($key, $value)
    {
        //do somehing
        if (intval($value)>1 and $value) {
            $this->whereArr[]=[$key,'=',intval($value)];
            //$this->whereArr[]='';
            return true;
        }

        return false;
    }
    
    /*
    *  @desc      获取日期 范围
    *  @author    saruri <saruri@163.com>
    *  @date      2020/10/26 14:35:15
    */
    public function getDate($key, $value)
    {
        //do somehing
        $arr=explode("-", $value);
        if (isset($arr[1]) and $value) {
            if (intval($arr[0]>0)) {
                $st=strtotime($arr[0].'-'.$arr[1].'-'.$arr[2]);
                $ed=strtotime($arr[3].'-'.$arr[4].'-'.$arr[5]);
                //$this->whereArr[]=[$key, 'between', [$st, $arr[1]]];
                $this->whereArr[]=[$key, '>=',$st];
                $this->whereArr[]=[$key, '<=',$ed];

                return true;
            }
        }
        return false;
    }

    /*
    *  @desc      获取字符串
    *  @author    saruri <saruri@163.com>
    *  @date      2020/10/26 14:35:15
    */
    public function getString($key, $value)
    {
        //do somehing
        //halt($value);
        if ($value) {
            $this->whereArr[]=[$key,'like','%' .$value. '%'];
            return true;
        }
        return false;
    }
}
