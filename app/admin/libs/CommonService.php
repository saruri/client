<?php

declare(strict_types=1);

namespace app\admin\libs;

use think\admin\Service as AdminService;
use app\admin\libs\PagerHelp;

/**
 * 通用服务类
 *
 * @Author NJW 2312449829@qq.com
 * @DateTime 2020-10-22 14:00:07
 */
class CommonService extends AdminService
{

    /**
     * 分页Html
     *
     * @Author NJW 2312449829@qq.com
     * @DateTime 2020-10-22 11:22:55
     * @return void
     */
    public function PagerHtml()
    {
        $result['pageHtml']=PagerHelp::instance()->GetPagerHtml($this->pager);
        $result['pageData']=PagerHelp::instance()->GetPagerData($this->pager);
        return $result;
    }
    /**
     * 分页Data
     *
     * @Author NJW 2312449829@qq.com
     * @DateTime 2020-10-22 11:23:22
     * @return array|void
     */
    public function PagerData()
    {
        $result=PagerHelp::instance()->GetPagerData($this->pager);
        return $result;
    }

    /* 
    *  @desc      指定字段转换为图片地址
    *  @author    saruri <saruri@163.com>
    *  @date      2021/04/08 14:14:40  
    */ 
    public function  pic($data=[],$field='cover')
    {
        //do somehing
        $list = $data['list'];
        $coverIds = array_column($list, $field);
        $covers = PictureIdsToUrl($coverIds);
        foreach ($list as $key => $value) {
            $list[$key][$field] = '';
            if (isset($covers[$value[$field]])) {
                $list[$key][$field] = $covers[$value[$field]];
            }
        }
        $data['list'] = $list;
        return $data;
    }

   /**
    * 设置属性
    * @author    saruri <saruri@163.com>
    * @date      2021/06/05 15:53:09
    * @auth true
    * @menu true
    * @login true
    * @throws \think\Exception
    * @throws \think\db\exception\DataNotFoundException
    * @throws \think\db\exception\DbException
    * @throws \think\db\exception\ModelNotFoundException
    */
    public function set(array $arr)
    {
        if (isset($arr)) {
            foreach ($arr as $key => $value) {
                if ($value) {
                    $this->$key=$value;
                    
                }
            }
        }
        return $this;
    }

    /**
     * 获取首字母
     *
     * @Author NJW 2312449829@qq.com
     * @DateTime 2020-12-18 14:14:16
     * @param [type] $str
     * @return void
     */
    public function GetFirstLetterByStr($str)
    {
        if (empty($str)) {
            return '';
        }

        $fchar = ord($str[0]);

        if ($fchar >= ord('A') && $fchar <= ord('z')) {
            return strtoupper($str[0]);
        }

        $s1 = iconv('UTF-8', 'gbk', $str);

        $s2 = iconv('gbk', 'UTF-8', $s1);

        $s = $s2 == $str ? $s1 : $str;

        $asc = ord($s[0]) * 256 + ord($s[1]) - 65536;

        if ($asc >= -20319 && $asc <= -20284) {
            return 'A';
        }

        if ($asc >= -20283 && $asc <= -19776) {
            return 'B';
        }

        if ($asc >= -19775 && $asc <= -19219) {
            return 'C';
        }

        if ($asc >= -19218 && $asc <= -18711) {
            return 'D';
        }

        if ($asc >= -18710 && $asc <= -18527) {
            return 'E';
        }

        if ($asc >= -18526 && $asc <= -18240) {
            return 'F';
        }

        if ($asc >= -18239 && $asc <= -17923) {
            return 'G';
        }

        if ($asc >= -17922 && $asc <= -17418) {
            return 'H';
        }

        if ($asc >= -17417 && $asc <= -16475) {
            return 'J';
        }

        if ($asc >= -16474 && $asc <= -16213) {
            return 'K';
        }

        if ($asc >= -16212 && $asc <= -15641) {
            return 'L';
        }

        if ($asc >= -15640 && $asc <= -15166) {
            return 'M';
        }

        if ($asc >= -15165 && $asc <= -14923) {
            return 'N';
        }

        if ($asc >= -14922 && $asc <= -14915) {
            return 'O';
        }

        if ($asc >= -14914 && $asc <= -14631) {
            return 'P';
        }

        if ($asc >= -14630 && $asc <= -14150) {
            return 'Q';
        }

        if ($asc >= -14149 && $asc <= -14091) {
            return 'R';
        }

        if ($asc >= -14090 && $asc <= -13319) {
            return 'S';
        }

        if ($asc >= -13318 && $asc <= -12839) {
            return 'T';
        }

        if ($asc >= -12838 && $asc <= -12557) {
            return 'W';
        }

        if ($asc >= -12556 && $asc <= -11848) {
            return 'X';
        }

        if ($asc >= -11847 && $asc <= -11056) {
            return 'Y';
        }

        if ($asc >= -11055 && $asc <= -10247) {
            return 'Z';
        }

        return "#";
    }

	/**
	 * service输出用
	 * @param bool $code
	 * @param string $msg
	 * @param array $data
	 * @DateTime 2021/4/8 14:30
	 * @author TG
	 * @return array
	 */
	public function serviceReturn(bool $code = true,string $msg = '成功',$data = []): array {
		return [
			'code' => $code,
			'msg' => $msg,
			'data' => $data
		];
	}

	/**
	 * 显示/隐藏文章
	 * @param mixed $channel_id 栏目id或表名
	 * @param int $id 文章id
	 * @return void
	 * @DateTime 2021/4/24 13:53
	 * @author TG
	 */
	public function setDisplay($channel_id,int $id) {
		if (is_int($channel_id)) {
			$tableList = config('app.channel_list');
			$table_name = $tableList[$channel_id]['table'];
		}else{
			$table_name = $channel_id;
		}
		$tableList = config('app.channel_list');
		$data = $this->app->db->name($table_name)->where(['id' => $id])->find();
		if (!$data) return;
		$status = $data['status'] == 1 ? 0 : 1;
		$this->app->db->name($table_name)->where(['id' => $id])->update(['status' => $status]);
	}

	/**
	 * 文章排序
	 * @DateTime 2021/4/24 14:05
	 * @param mixed $channel_id
	 * @param int $id
	 * @param int $sort
	 * @return void
	 * @author TG
	 */
	public function sortArticle($channel_id,int $id,int $sort) {
		if (is_int($channel_id)) {
			$tableList = config('app.channel_list');
			$table_name = $tableList[$channel_id]['table'];
		}else{
			$table_name = $channel_id;
		}
		$this->app->db->name($table_name)->where(['id' => $id])->update(['sort' => $sort]);
	}
}
