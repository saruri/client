<?php

declare(strict_types=1);

namespace app\admin\libs;

use think\admin\Service as AdminService;

class PagerHelp extends AdminService
{

    
    /**
     * 分页数据转HTML
     *
     * @Author NJW 2312449829@qq.com
     * @DateTime 2020-10-22 10:34:33
     * @param [type] $pager
     * @return void
     */
    public function GetPagerHtml($pager)
    {
        $limit = intval($pager->listRows());
        [$options, $query] = ['', $this->app->request->get()];
        foreach ([2,10, 20, 30, 40, 50, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150, 160, 170, 180, 190, 200] as $num) {
            [$query['limit'], $query['page'], $selects] = [$num, 1, $limit === $num ? 'selected' : ''];
            if (stripos($this->app->request->get('spm', '-'), 'm-') === 0) {
                $url = sysuri('admin/index/index') . '#' . $this->app->request->baseUrl() . '?' . urldecode(http_build_query($query));
            } else {
                $url = $this->app->request->baseUrl() . '?' . urldecode(http_build_query($query));
            }
            $options .= "<option data-num='{$num}' value='{$url}' {$selects}>{$num}</option>";
        }
        $selects = "<select onchange='location.href=this.options[this.selectedIndex].value' data-auto-none>{$options}</select>";
        $pagetext = lang('think_library_page_html', [$pager->total(), $selects, $pager->lastPage(), $pager->currentPage()]);
        $pagehtml = "<div class='pagination-container nowrap'><span>{$pagetext}</span>{$pager->render()}</div>";
        if (stripos($this->app->request->get('spm', '-'), 'm-') === 0) {
            return preg_replace('|href="(.*?)"|', 'data-open="$1" onclick="return false" href="$1"', $pagehtml);
        } else {
            return $pagehtml;
        }
    }
    /**
     * 分页数据格式化
     *
     * @Author NJW 2312449829@qq.com
     * @DateTime 2020-10-22 10:34:21
     * @param Pager $pager
     * @return void
     */
    public function GetPagerData($pager)
    {
        $result = ['page' => ['limit' => intval($pager->listRows()), 'total' => intval($pager->total()), 'pages' => intval($pager->lastPage()), 'current' => intval($pager->currentPage())], 'list' => $pager->items()];
        return $result;
    }
}
