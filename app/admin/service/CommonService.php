<?php

declare(strict_types=1);

namespace app\admin\service;

use think\admin\Service as AdminService;
use app\admin\libs\PagerHelp;
use app\admin\libs\WhereOrService;
/**
 * 通用服务类
 *
 * @Author NJW 2312449829@qq.com
 * @DateTime 2020-10-22 14:00:07
 */
class CommonService extends AdminService
{

    /**
     * @desc      
     * @author    刘国成 <614998074@qq.com>
     * @date      2020/11/20 15:02
     */
    protected function initialize()
    {
        $this->page=1;
        $this->limit = 10;//Config::get('app.limit');
    }
    /**
     * 分页Html
     *
     * @Author NJW 2312449829@qq.com
     * @DateTime 2020-10-22 11:22:55
     * @return void
     */
    public function PagerHtml()
    {
        $result['pageHtml']=PagerHelp::instance()->GetPagerHtml($this->pager);
        $result['pageData']=PagerHelp::instance()->GetPagerData($this->pager);
        return $result;
    }
    /**
     * 分页Data
     *
     * @Author NJW 2312449829@qq.com
     * @DateTime 2020-10-22 11:23:22
     * @return void
     */
    public function PagerData()
    {
        $result=PagerHelp::instance()->GetPagerData($this->pager);
        return $result;
    }



    /**
     * 分页Data 修改分页
     *
     * @Author NJW 2312449829@qq.com
     * @DateTime 2020-10-22 11:23:22
     * @return void
     */
    public function Pages($table=[],$where=[],$tableField=[],$search=[],$order=[],$whereOr=[])
    {
        if (isset($search['limit'])) {
            $this->limit = $search['limit'];
        }
        $where_new=WhereService::instance()->set($search)->common()->index();
        $where_or=WhereOrservice::instance()->set($where_new)->common()->index();
       
       if(empty($where_or)){
        $this->pager = $this->app->db->name($table)->where($where)->where($where_new)->whereOr($where_or)->order($order)->field($tableField)->paginate(['list_rows' => $this->limit,'query'=>$this->app->request->get()], false);
       }

       if(!empty($where_or)){
        $this->pager = $this->app->db->name($table)->where($where)->whereOr([$where_new,$where_or])->order($order)->field($tableField)->paginate(['list_rows' => $this->limit,'query'=>$this->app->request->get()], false);
       }
        //halt($this->app->db->getLastSql());
        $finalWhere['new']= $where_new;
        $finalWhere['or']= $where_or;
        $finalWhere['where']=$where;
        $finalWhere['sql']=$this->app->db->getLastSql();
        // halt($finalWhere);
        return $this;
    }
}
