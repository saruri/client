<?php

declare(strict_types=1);

namespace app\admin\service;

use app\admin\libs\PagerHelp;
use think\facade\Cache;
use app\admin\libs\CommonService;

class PictureService extends CommonService
{


    /**
     * 图片库表
     *
     * @var string
     * @Author NJW 2312449829@qq.com
     * @DateTime 2021-04-07 10:54:51
     */
    protected $table = 'image';
    /**
     * 图片地址
     *
     * @var string
     * @Author NJW 2312449829@qq.com
     * @DateTime 2021-04-07 10:54:32
     */
    protected $imageurl = '';
    protected function initialize()
    {
        $this->limit = config('app.limit');
        $this->page = 1;
        $this->imageurl = config('app.imageurl');
    }
    /**
     * 图片ID转图片地址
     *
     * @Author NJW 2312449829@qq.com
     * @DateTime 2021-04-07 10:57:59
     * @param integer $id 图片地址数组,如 1
     * @param bool $compressFlag 是否返回压缩图 true返回 false 返回原图 默认ture 
     * @return string 图片地址，如https://xxxx.com/upload/cd/ddd.png
     */
    public function PictureIdToUrl(int $id, bool $compressFlag = true)
    {
        $where[] = ['is_delete', '=', '0'];
        $where[] = ['id', '=', $id];
        $url = $this->app->db->name($this->table)->where($where)->find();
        if (empty($url)) {
            return '';
        }
        if ($compressFlag) {
            return $this->imageurl . $url['compress_url'];
        }
        return $this->imageurl . $url['url'];
    }
    /**
     * 图片ID数组转图片地址
     *
     * @Author NJW 2312449829@qq.com
     * @DateTime 2021-04-07 10:52:53
     * @param array $ids 图片ID数组,如[1,2,3]
     * @param bool $compressFlag 是否返回压缩图 true返回 false 返回原图 默认ture 
     * @return array 图片ID为索引的地址数组，如[1=>https://xxxx.com/upload/cd/ddd.png]
     */
    public function PictureIdsToUrl(array $ids, bool $compressFlag = true)
    {
        $where[] = ['is_delete', '=', '0'];
        $where[] = ['id', 'in', $ids];
        $urls = $this->app->db->name($this->table)->where($where)->column('*', 'id');
        foreach ($urls as $key => $value) {
            if ($compressFlag) {
                $urls[$key] = $this->imageurl . $value['compress_url'];
            }else{
                $urls[$key] = $this->imageurl . $value['url'];
            }
        }
        return $urls;
    }

    /**
     * 添加图片
     *
     * @Author NJW 2312449829@qq.com
     * @DateTime 2021-04-07 11:09:15
     * @param string $imageurl 图片地址
     * @param bool $compressFlag 是否返回压缩图
     * @return int 图片ID
     */
    public function PictureInsert(string $imageurl, $compressFlag)
    {
        $where[] = ['is_delete', '=', '0'];
        $where[] = ['url', '=', $imageurl];
        $id = $this->app->db->name($this->table)->where($where)->value("id");
        if ($id !== null) {
            return $id;
        }

        $data['compress_url'] = $imageurl;
        if ($compressFlag) {
            $data['compress_url'] = 'compress/' . $imageurl;
        }
        $data['url'] = $imageurl;
        $data['create_time'] = time();
        $data['is_delete'] = 0;
        $id = $this->app->db->name($this->table)->insertGetId($data);
        return $id;
    }

    // /**
    //  * 获取分页数据
    //  *
    //  * @Author NJW 2312449829@qq.com
    //  * @DateTime 2020-10-22 11:17:58
    //  * @param array $jsonData
    //  * @return $this
    //  */
    // public function GetCompanyData($jsonData)
    // {
    //     $where[] = ['is_delete', '=', '0'];
    //     $order = ['id' => 'DESC'];
    //     $query = $this->app->db->name($this->companyTable)->where($where);
    //     //where条件
    //     if (isset($jsonData['where'])) {
    //         $query->where($jsonData['where']);
    //     }
    //     //排序
    //     if (isset($jsonData['order'])) {
    //         $query->order($jsonData['order']);
    //     }
    //     $query->order($order);
    //     //每页数量
    //     if (isset($jsonData['limit'])) {
    //         $this->limit = $jsonData['limit'];
    //     }
    //     if (isset($jsonData['page'])) {
    //         $this->page = $jsonData['page'];
    //         $this->pager = $query->paginate(['list_rows' => $this->limit, 'page' => $this->page], false);
    //         return $this;
    //     }
    //     $this->pager = $query->paginate(['list_rows' => $this->limit, 'query' => $this->app->request->get()], false);
    //     return $this;
    // }

}
