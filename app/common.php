<?php
// 这是系统自动生成的公共文件
// if(!function_exists('test')){
//     function test(){
//         echo 'test';
//     }
// }
use app\admin\service\PictureService;
use think\facade\Db;

/*
*  @desc      获取单个图片输出为对象
*  @author    saruri <saruri@163.com>
*  @date      2021/08/16 11:14:26
*/
function getSingleImgObject(string $ids)
{
    //do somehing
    $data=pictureIdsToUrlArray($ids);
    $data[0]['imageId']=$data[0]['id'];
    $data[0]['url']=$data[0]['cover'];
    unset($data[0]['cover']);
    unset($data[0]['id']);
    return (object) $data[0];
}


/*
*  @desc      用id获取 name
*  @author    saruri <saruri@163.com>
*  @date      2021/08/16 11:30:26
*/
function getName($table, $id)
{
    $where=[
        'id'=>$id
    ];
    $field='id,name';
    $data=Db::table($table)->field($field)->where($where)->find();
    return $data;
}


/**
 * 图片ID转图片地址
 *
 * @Author NJW 2312449829@qq.com
 * @DateTime 2021-04-07 10:57:59
 * @param bool $compressFlag 是否返回压缩图 true返回 false 返回原图 默认ture
 * @param integer $id 图片地址数组,如 1
 * @return string 图片地址，如https://xxxx.com/upload/cd/ddd.png
 */
function pictureToUrl(int $id, bool $compressFlag = true)
{
    $url = PictureService::instance()->PictureIdToUrl($id, $compressFlag);
    return $url;
}

/**
 * 图片ID数组转图片地址
 *
 * @Author NJW 2312449829@qq.com
 * @DateTime 2021-04-07 10:52:53
 * @param array $ids 图片ID数组,如[1,2,3]
 * @param bool $compressFlag 是否返回压缩图 true返回 false 返回原图 默认ture
 * @return array 图片ID为索引的地址数组，如[1=>https://xxxx.com/upload/cd/ddd.png]
 */
function PictureIdsToUrl(array $ids, bool $compressFlag = true)
{
    $urls = PictureService::instance()->PictureIdsToUrl($ids, $compressFlag);
    return $urls;
}

/**
 * 图片id字符串或数组转图片地址
 * @param array|string $ids
 * @DateTime 2021/4/9 11:22
 * @author TG
 * @return array
 */
function pictureIdsToUrlArray($ids): array
{
    if ($ids == '' || $ids == []) {
        return [];
    }
    if (is_string($ids) || is_int($ids)) {
        $ids = explode(',', $ids);
    }
    $urls = PictureService::instance()->PictureIdsToUrl($ids);
    $arr = [];
    foreach ($urls as $c_k => $c_v) {
        $arr[] = [
            'id' => $c_k,
            'cover' => $c_v
        ];
    }
    return $arr;
}

/**
 * 添加图片
 *
 * @Author NJW 2312449829@qq.com
 * @DateTime 2021-04-07 11:09:15
 * @param string $imageurl 图片地址
 * @return int 图片ID
 */
function PictureInsert(string $imageurl, bool $compressFlag)
{
    $id = PictureService::instance()->PictureInsert($imageurl, $compressFlag);
    return  $id;
}

/**
 * 时间戳转时间
 * @param string|array $data
 * @DateTime 2021/4/8 16:47
 * @author TG
 * @return mixed
 */
function timeToFormart($data)
{
    if (is_string($data)) {
        $data = date('Y-m-d H:i:s', $data);
    }
    if (count($data)) {
        if (isset($data[0]['create_time'])) {
            foreach ($data as &$d) {
                $d['create_time'] = date('Y-m-d H:i:s', $d['create_time']);
            }
        }
        if (isset($data[0]['update_time'])) {
            foreach ($data as &$d) {
                $d['update_time'] = date('Y-m-d H:i:s', $d['update_time']);
            }
        }
    }
    return $data;
}

/**
 * 二维数组搜索
 * @param array $array 带查询的数组
 * @param string $key 键名
 * @param string $value 键值
 * @DateTime 2021/4/10 16:45
 * @return mixed 键名对应的那条数据
 * @author TG
 */
function searchArray(array $array, string $key, string $value)
{
    foreach ($array as $key_i => $value_i) {
        if ($value_i[$key] == $value) {
            return $array[$key_i];
        }
    }
    return false;
}

/**
 * 二维数组根据某个字段排序
 * @param array $array 要排序的数组
 * @param string $keys 要排序的键
 * @param string $sort 排序方式(desc asc)
 * @DateTime 2021/4/10 17:44
 * @return array
 * @author TG
 */
function sortArray(array $array, string $keys, string $sort = 'desc')
{
    $newArr = $valArr = array();
    foreach ($array as $key => $value) {
        $valArr[$key] = $value[$keys];
    }
    ($sort == 'asc') ? asort($valArr) : arsort($valArr);
    reset($valArr);
    foreach ($valArr as $key => $value) {
        $newArr[$key] = $array[$key];
    }
    return array_values($newArr);
}


/**
 * 富文本中的图片替换
 *
 * @Author NJW 2312449829@qq.com
 * @DateTime 2021-04-13 19:50:10
 * @param [string] $content 要替换的内容
 * @return string
 */
function updateContentImgUrl($content)
{
    $arr = [];
    preg_match_all('/src=\"?(.+\.(jpg|gif|bmp|bnp|png))\"/i', $content, $arr);
    // halt($arr[1]);
    foreach ($arr[1] as $key => $value) {
        $url = explode('upload/', $value);
        //   halt($url);
        if (!isset($url[1])) {
            continue;
        }
        $content = str_replace($value, config('app.imageurl') . $url[1], $content);
    }
    // halt($arr);
    return $content;
}


//数组转对象
function array2object($array)
{
    if (is_array($array)) {
        $obj = new StdClass();

        foreach ($array as $key => $val) {
            $obj->$key = $val;
        }
    } else {
        $obj = $array;
    }

    return $obj;
}


//对象转数组
function object2array($object)
{
    if (is_object($object)) {
        foreach ($object as $key => $value) {
            $array[$key] = $value;
        }
    } else {
        $array = $object;
    }
    return $array;
}


//把图片id  转换为 图
function addImg($data)
{
    if (isset($data['img'])) {
        $data['img'] = pictureIdsToUrlArray($data['img']);
        //统一字段规范
        foreach ($data['img'] as &$img) {
            $img = [
                        'id' => $img['id'],
                        'url' => $img['cover']
            ];
        }
    }
    return $data;
}
