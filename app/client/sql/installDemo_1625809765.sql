CREATE TABLE IF NOT EXISTS `install_demo` ( `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
 `sort` int(11) NOT NULL  DEFAULT '0'  COMMENT '排序 ',
`name`  varchar(200) NOT NULL DEFAULT '' COMMENT '名称 ',
 `st` tinyint(1) NOT NULL  DEFAULT '0'  COMMENT '是否开启 ',
 `lession` int(1) NOT NULL  DEFAULT '0'  COMMENT '课程 {课程1 0,课程2 1,课程3 2}',
 `sex` int(1) NOT NULL  DEFAULT '0'  COMMENT '性别 女 0 ,男 1',
 `vip` int(3) NOT NULL  DEFAULT '0'  COMMENT 'vip ',
`dev`  varchar(100) NOT NULL DEFAULT '' COMMENT '设备 苹果 1,安卓 2,电脑 3',
`sub`  varchar(100) NOT NULL DEFAULT '' COMMENT '指导老师 ',
`from`  varchar(100) NOT NULL DEFAULT '' COMMENT '来源 ',
 `teacher` int(4) NOT NULL  DEFAULT '0'  COMMENT '教师id 李老师 1,张老师 2,王老师 3',
 `leader` int(4) NOT NULL  DEFAULT '4'  COMMENT '班主任 ',
 `face` int(11) NOT NULL  DEFAULT '0'  COMMENT '照片 ',
 `class` int(4) NOT NULL  DEFAULT '0'  COMMENT '班级 ',
 `file` int(11) NOT NULL  DEFAULT '0'  COMMENT '简历文档 ',
`con`  TEXT NOT NULL DEFAULT '' COMMENT '介绍  ',
`desc`  TEXT NOT NULL DEFAULT '' COMMENT '简介  ',
 `creat_time` int(10) NOT NULL  DEFAULT '0'  COMMENT '创建时间 ',
 `begin` int(11) NOT NULL  DEFAULT '0'  COMMENT '入职时间 ',
PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COMMENT=' 表名 表';
        
        