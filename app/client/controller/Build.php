<?php
declare (strict_types = 1);

// +----------------------------------------------------------------------
// | Saruri
// +----------------------------------------------------------------------
// | 版权所有 2020~2021 系统集成中国有限公司 [ http://www.saruri.cn ]
// +----------------------------------------------------------------------
// | 官方网站: https://saruri.cn
// +----------------------------------------------------------------------
// | 开源协议 ( https://mit-license.org )
// +----------------------------------------------------------------------
// | gitee 代码仓库：https://gitee.com/saruri/saruri.git
// | github 代码仓库：https://github.com/sarurifan/saruri.git
// +----------------------------------------------------------------------

namespace app\client\controller;

use think\Request;
use app\admin\libs\CommonController;
use app\admin\service\BuildService;
use app\admin\service\MakeService;
/**
 * 代码生成的Build 控制器
 * Class Build 
 * @package app\client\controller
 */
class Build extends CommonController 
{
    
	/**
	 * 绑定数据表
	 * @var string
	 */
	public $table = 'Build';

	/**
	* 首页
	* @date      2021-06-24 09:54:24
	* @auth true
	* @menu true
	* @login true
	* @throws \think\Exception
	* @throws \think\db\exception\DataNotFoundException
	* @throws \think\db\exception\DbException
	* @throws \think\db\exception\ModelNotFoundException
    */ 
	public function index() {
       
		if ($this->request->isGet()) {
			$this->fetch('index');
		} else {
			$this->like('name#name');
			$this->timeBetween('create_time#time');
			$jsonData['where'] = $this->queryWhere;
			
			$jsonData['limit'] = $this->app->request->request('limit');
			$jsonData['page'] = $this->app->request->request('page');
			$result = BuildService::instance()->list($jsonData);

			$arr["code"]= 0; //解析接口状态
			$arr["info"]= "chenggong"; //解析提示文本
			$arr["count"]= 10; //解析数据长度
			$arr["data"]['list']= $result; //解析数据列表
			
			$this->success('成功', $result);
		}
	}

	/**
	* 删除
	* @author    saruri <saruri@163.com>
	* @date      2021-06-24 09:54:24
	* @auth true
	* @menu true
	* @login true
	* @throws \think\Exception
	* @throws \think\db\exception\DataNotFoundException
	* @throws \think\db\exception\DbException
	* @throws \think\db\exception\ModelNotFoundException
	*/ 
	public function  del()
	{
		$id = $this->request->post('id', 0);

		$result = BuildService::instance()->del($id);
		if ($result) {
			$this->success('删除成功');
		} else {
			$this->error('删除失败');
		}
		 
	}

    /**
	* 增加修改
	* @author    saruri <saruri@163.com>
	* @date      2021-06-24 09:54:24
	* @auth true
	* @menu true
	* @login true
	* @throws \think\Exception
	* @throws \think\db\exception\DataNotFoundException
	* @throws \think\db\exception\DbException
	* @throws \think\db\exception\ModelNotFoundException
	*/ 
	public function  add()
	{
		//do somehing
		if ($this->request->isPost()) {
			$data = input('data');
			unset($data['id']);
			$res = BuildService::instance()->add($data);
			$res? $this->success('成功') : $this->error('失败');
		} else {

			if ($this->request->get('output') === 'json') {
			$result=  BuildService::instance()->get(['id' => input('id')]);
			$this->success('获取数据成功！', $result);
			}

			$company=[
				'0'=>[
					'id'=>0,
					'title'=>'演示公司1'
				],
				'1'=>
				['id'=>1,
				'title'=>'演示公司2'
				]
			];
			$this->assign('company',$company);
			$this->fetch('form');
		}
		 
	}



	/**
	* 构造
	* @author    saruri <saruri@163.com>
	* @date      2021/06/27 23:35:34  
	*/ 
	public function  make()
	{
		//do somehing
		$data=input('data');
		//halt($data);
		$arr['data']=$data;
		$result=MakeService::instance()->set($arr)->index();
		$this->success('成功!',$result);
		exit("这里是:");
		 
	}

}
