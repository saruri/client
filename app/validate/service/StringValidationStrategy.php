<?php

namespace app\validate\service;

use app\validate\service\ValidationStrategy;

class StringValidationStrategy implements ValidationStrategy
{
    public function validate($value, $options = [])
    {
        //不检测字符串了 麻烦
        return true;
        // 首先确保$value是一个字符串
        if (!is_string($value)) {
            return false;
        }

        // 检查字符串长度
        $length = strlen($value);
        if (isset($options['min_length']) && $length < $options['min_length']) {
            return false;
        }
        if (isset($options['max_length']) && $length > $options['max_length']) {
            return false;
        }

        // 如果设置了正则表达式，则检查是否匹配
        if (isset($options['pattern']) && !preg_match($options['pattern'], $value)) {
            return false;
        }

        // 如果设置了一个可接受的字符集，则检查字符串是否符合要求
        if (isset($options['allowed_chars']) && !preg_match('/^[' . preg_quote($options['allowed_chars'], '/') . ']+$/u', $value)) {
            return false;
        }

        return true;
    }
}
