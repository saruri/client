<?php

// +----------------------------------------------------------------------
// | ThinkXinrun
// +----------------------------------------------------------------------
// | 版权所有 2020 唐山鑫软网络科技有限公司 [ http://www.tsxrwl.com ]
// +----------------------------------------------------------------------
// | 官方网站: https://www.tsxrwl.com
// +----------------------------------------------------------------------

namespace app\validate\service;
use app\validate\service\ValidationStrategy;

class MinLengthValidationStrategy implements ValidationStrategy
{
    public function validate($value, $options = [])
    {
        return strlen($value) >= intval($options);
    }
}