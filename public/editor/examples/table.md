
# 表名 installDemo
### 说明 这个表用来生成你的模块了 {forceApiPath:api}
| 字段名  |说明  |  类型 |长度   | 元素  |可选   |默认值  |来源表   | 必填  |过滤器|
| --| --| --| --| --| --| --| --| --| --
| id  |id   | int  |11   |  id |  主键/自增 |  |   |||
| sort | 排序   | int   | 11  |  input |    |   ||是| int|
| create_time | 创建时间 | int | 10 | datetime || now || 是 | int |
| is_enable | 是否开启 | tinyint | 1 | switch ||0||| int |
| name | 名称   |varchar   | 200  |  input |    |   ||是||
| lession | 课程 | int | 1 | radio | {课程1 0,课程2 1,课程3 2} | 0 || 是 | int |
| sex | 性别 | int | 1 | radio | 女 0 ,男 1 | 0 ||||
| vip | vip | int | 3 | radio_table ||| teacher,teacher_name,id | 是 | int |
| dev | 设备 | varchar | 100 | checkbox | 苹果 1,安卓 2,电脑 3|||||
| sub | 指导老师 | varchar | 100 | checkbox_table ||| teacher,teacher_name,id |||
| from | 来源 | varchar | 100 | radio_table ||| teacher,teacher_name,id |||
| teacher | 教师id | int | 4 | select | 李老师 1,张老师 2,王老师 3|||||
| leader | 班主任| int | 4 | select_table || 4 | teacher,teacher_name,id | 是 | int |
| face | 照片 | varchar | 200 | upload_img |||| 否 | |
| class | 班级| int | 4 | select_table || 0 | teacher,teacher_name,id | 是 | int |
| file1 | 简历文档 | varchar | 200 | upload_file |||| 否 |  |
| con | 介绍 | text || editor ||||||
| desc | 简介 | text || editor_textarea |||| 否 ||
| begin | 入职时间 | int | 11 | carlendar |||| 否 | int |
---

# 简单说明
```
id ,sort,create_time,is_enable 为基础字段 必选
file 为保留字段 也不能用
在说明字段中 加入   {override} 为强制更新表  请慎用!
在说明字段中 加入  {forceUpdate} 为强制更新文件 请慎用!
在说明字段中 加入 {forcePath:sms} 为强制指定模块的路径 请慎用!
增加API生成路径 {forceApiPath:api} 为强制指定api的路径 请慎用!
过滤器 分为类型 int email phone
辅助过滤器 为 最大长度max , 最小长度 min ,数字区间 between
必填 required 为false 的时候 忽略为空错误
示例
int,min:1,max:10
email
phone
int,between:1-10
建议在本地php 运行客户端 进行操作 纳入版本管理,在服务器上操作 请确保不会版本冲突
点击 对应格式的按钮 可以生成固定格式的字段 然后改写 提交即可



```
```
php 验证器参数实例
	$param = [
            'name' => [
                'type' => 'string',
                'required' => true,
                'minLength' => 1,
                'maxLength' => 20
            ],
            'email' => [
                'type' => 'email',
                'required' => true
            ],
            'age' => [
                'type' => 'int',
                'required' => true,
				'minLength' => 2,
                'maxLength' => 20
            ],
			'phone' => [
                'type' => 'phone',
                'required' => true
            ],
			'price' => [
                'type' => 'int',
                'required' => false,
				'numberBetween'=>[20,100]
            ]

        ];

```
```
接口传参
{
    "name":"范子平",
    "email":"sa@163.com",
    "age":21,
    "phone":1398881,
    "price":91
}
```